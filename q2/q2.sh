#!/bin/bash

read -p "Digite o nome do primeiro diretório: " d1
read -p "Digite o nome do segundo diretório: " d2
read -p "Digite o nome do terceiro diretório: " d3

qx1=$(find "$d1" -type f -name "*.xls" | wc -l)
qb1=$(find "$d1" -type f -name "*.bmp" | wc -l)
qd1=$(find "$d1" -type f -name "*.docx" | wc -l)

qx2=$(find "$d2" -type f -name "*.xls" | wc -l)
qb2=$(find "$d2" -type f -name "*.bmp" | wc -l)
qd2=$(find "$d2" -type f -name "*.docx" | wc -l)

qx3=$(find "$d3" -type f -name "*.xls" | wc -l)
qb3=$(find "$d3" -type f -name "*.bmp" | wc -l)
qd3=$(find "$d3" -type f -name "*.docx" | wc -l)

echo "No diretório '$d1':"
echo "Arquivos .xls: $qx1"
echo "Arquivos .bmp: $qb1"
echo "Arquivos .docx: $qd1"

echo "No diretório '$d2':"
echo "Arquivos .xls: $qx2"
echo "Arquivos .bmp: $qb2"
echo "Arquivos .docx: $qd2"

echo "No diretório '$d3':"
echo "Arquivos .xls: $qx3"
echo "Arquivos .bmp: $qb3"
echo "Arquivos .docx: $qd3"

