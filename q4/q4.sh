#!/bin/bash

echo -e "Uso de memória: "
free

echo -e "Uso de disco: "
df

echo -e "Usuários logados:"
who

echo -e "Número de arquivos e diretórios na pasta atual:"
ls -1 | wc -l

echo -e "Número de arquivos e diretórios na pasta home:"
ls -1 ~ | wc -l

echo "Processador:"
lscpu | grep "Model name"

echo "Memória RAM:"
free -h | grep "Mem:"

echo "Placa-mãe:"
sudo dmidecode -t baseboard | grep "Manufacturer\|Product Name"

echo "Dispositivos de armazenamento:"
lsblk

echo "Placas de vídeo:"
lspci | grep VGA

echo "Dispositivos USB conectados:"
lsusb

echo "Interfaces de rede:"
ip a

echo "Sistema Operacional:"
lsb_release -a

