#!/bin/bash

nu1="$1"
nu2="$2"
nu3="$3"

re1=$(echo "scale=2; $nu1 * $nu2 * $nu3" | bc)

echo "O resultado da multiplicação é: $re1"
