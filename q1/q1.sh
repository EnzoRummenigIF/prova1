#!/bin/bash


echo "Estou hoje dividido entre a lealdade que devo"
sleep 3
echo "À Tabacaria do outro lado da rua, como coisa real por fora,"
sleep 3
echo "E à sensação de que tudo é sonho, como coisa real por dentro."
sleep 3
echo "Falhei em tudo."
sleep 3
echo "Como não fiz propósito nenhum, talvez tudo fosse nada."
sleep 3
echo "A aprendizagem que me deram,"
sleep 3
echo "Desci dela pela janela das traseiras da casa,"
sleep 3
echo "Fui até ao campo com grandes propósitos."
sleep 3
echo "Mas lá encontrei só ervas e árvores,"
sleep 3
echo "E quando havia gente era igual à outra."
