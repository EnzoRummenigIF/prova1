#!/bin/bash


n="Enzo"
echo "1. Variável com valor atribuído diretamente: \$nome = $n"

echo "2. Digite seu sobrenome:"
read s
echo "Variável lida do usuário: \$sobrenome = $s"

p1="$1" 
p2="$2" 

echo "3. Valores passados como parâmetros de linha de comando:"
echo "\$parametro1 = $p1"
echo "\$parametro2 = $p2"

va="$USER" 
echo "4. Variável de ambiente: \$USER = $va"

echo "5. Variáveis especiais:"
echo "\$0 (Nome do script): $0"
echo '\$# (Número de argumentos): $#'
echo "\$? (Código de retorno do último comando): $?"
echo "\$! (PID do último processo em segundo plano): $!"

echo "6. Variáveis de Shell:"
echo "\$SHELL (Shell padrão): $SHELL"
echo "\$HOME (Diretório home do usuário): $HOME"

echo "1. Atribuindo um valor diretamente"
echo "2. Lendo um valor do usuário"
echo "3. Recebendo valores como parâmetros de linha de comando"
echo "4. Variáveis de ambiente"
echo "5. Variáveis especiais"
echo "6. Variáveis de Shell"

